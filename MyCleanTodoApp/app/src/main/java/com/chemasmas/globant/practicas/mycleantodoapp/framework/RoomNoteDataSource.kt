package com.chemasmas.globant.practicas.mycleantodoapp.framework

import android.content.Context
import com.chemasmas.globant.practicas.core.data.Note
import com.chemasmas.globant.practicas.core.repository.NoteDatasource
import com.chemasmas.globant.practicas.mycleantodoapp.framework.db.DatabaseService
import com.chemasmas.globant.practicas.mycleantodoapp.framework.db.NoteEntity

class RoomNoteDataSource(context: Context):NoteDatasource {

    val nodeDao = DatabaseService.getInstance(context).noteDAO()

    override suspend fun add(note: Note) {
        nodeDao.addNoteEntity(
            NoteEntity.fromNote(note)
        )
    }

    override suspend fun get(id: Long): Note? {
        return nodeDao.getNoteEntity(id)?.toNote()
    }

    override suspend fun getAll(): List<Note> {
        return nodeDao.getAllNoteEntity().map {
            it.toNote()
        }
    }

    override suspend fun remove(note: Note) {
        nodeDao.deleteNoteEntity(
            NoteEntity.fromNote(note)
        )
    }
}