package com.chemasmas.globant.practicas.core.repository

import com.chemasmas.globant.practicas.core.data.Note

interface NoteDatasource {
    suspend fun add(note: Note)
    suspend fun get(id:Long): Note?
    suspend fun getAll():List<Note>
    suspend fun remove(note:Note):Unit
}