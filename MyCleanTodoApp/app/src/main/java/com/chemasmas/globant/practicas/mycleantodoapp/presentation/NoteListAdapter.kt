package com.chemasmas.globant.practicas.mycleantodoapp.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.chemasmas.globant.practicas.core.data.Note
import com.chemasmas.globant.practicas.mycleantodoapp.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NoteListAdapter(var notes:ArrayList<Note>, val action:ListAction ): RecyclerView.Adapter<NoteListAdapter.NoteViewHolder>() {

    fun updateNotes(newNotes:List<Note>){
        notes.clear()
        notes.addAll(newNotes)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = NoteViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_note,parent, false)
    )

    override fun getItemCount(): Int {
        return notes.size
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind( notes[position] )
    }


    inner class NoteViewHolder(view: View):RecyclerView.ViewHolder(view){
        val title: TextView = view.findViewById(R.id.title)
        val content: TextView = view.findViewById(R.id.content)
        val date: TextView = view.findViewById(R.id.date)
        val wordCount: TextView = view.findViewById(R.id.wordCount)
        val layout:View = view

        fun bind(note: Note){
            title.text = note.title
            content.text = note.content
            val sdf = SimpleDateFormat("MMM dd, HH:mm:ss")
            val d  = Date(note.updateTime)
            date.text = "Last Update ${sdf.format(d)}"

            wordCount.text = "Words ${note.wordCount}"

            layout.setOnClickListener {
                action.onClick(note._id)
            }
        }
    }

}