package com.chemasmas.globant.practicas.mycleantodoapp.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chemasmas.globant.practicas.mycleantodoapp.R
import com.chemasmas.globant.practicas.mycleantodoapp.framework.ListNoteViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ListFragment : Fragment(),ListAction {

    private lateinit var viewModel: ListNoteViewModel

    lateinit var recyclerView:RecyclerView
    lateinit var loader:ProgressBar
    lateinit var addNote:FloatingActionButton
    val adapter = NoteListAdapter(arrayListOf(),this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addNote = view.findViewById(R.id.addNote)
        recyclerView = view.findViewById(R.id.notesRV)
        loader = view.findViewById(R.id.loadingView)

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter

        addNote.setOnClickListener{
            goToNoteDetails()
        }

        viewModel = ViewModelProvider(this)[ListNoteViewModel::class.java]

        observeViewModel()
        viewModel.getAllNotes()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAllNotes()
    }

    private fun observeViewModel() {
        viewModel.notas.observe(requireActivity()){
            adapter.updateNotes(it.sortedByDescending { it.updateTime })
            loader.isVisible = false
        }
    }

    private fun goToNoteDetails(id:Long = 0L){
        val action = ListFragmentDirections.actionGoToNote(id)
        Navigation.findNavController(addNote).navigate(action)
    }

    override fun onClick(id: Long) {
        goToNoteDetails(id)
    }
}