package com.chemasmas.globant.practicas.core.usecase

import com.chemasmas.globant.practicas.core.repository.NoteRepository

class GetNote(private val noteRepository: NoteRepository) {

    suspend operator fun invoke(id:Long) = noteRepository.getNote(id)
}