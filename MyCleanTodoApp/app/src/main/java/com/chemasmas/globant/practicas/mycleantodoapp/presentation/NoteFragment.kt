package com.chemasmas.globant.practicas.mycleantodoapp.presentation

import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.chemasmas.globant.practicas.core.data.Note
import com.chemasmas.globant.practicas.mycleantodoapp.R
import com.chemasmas.globant.practicas.mycleantodoapp.framework.NoteViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class NoteFragment : Fragment() {


    private var noteId = 0L
    private lateinit var viewModel: NoteViewModel
    var currentNote = Note("","",0L,0L)

    lateinit var save: FloatingActionButton
    lateinit var titleView:TextView
    lateinit var contentView:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_note, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(object:MenuProvider{
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.note_menu, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                when(menuItem.itemId){
                    R.id.deleteNote -> {
                        if( context != null && noteId != 0L ){
                            AlertDialog.Builder(context!!)
                                .setTitle("Delet Note")
                                .setMessage("Are you sure about deleting this note?")
                                .setPositiveButton("Yes!"){ dialogInterface,i ->
                                    viewModel.deleteNote(currentNote)
                                }
                                .setNegativeButton("No"){ dialogInterface,i ->

                                }
                                .create()
                                .show()
                        }

                    }
                    else -> {
                        //TODO
                    }
                }
                return true
            }

        },viewLifecycleOwner, Lifecycle.State.RESUMED)

        viewModel = ViewModelProvider(this).get(NoteViewModel::class.java)

        arguments?.let{
            noteId = NoteFragmentArgs.fromBundle(it).noteID
        }
        if(noteId != 0L){
            viewModel.getNote(noteId)
        }

        save = view.findViewById(R.id.saveButton)
        titleView = view.findViewById(R.id.titleView)
        contentView = view.findViewById(R.id.contentView)

        save.setOnClickListener{
            if(titleView.text.toString() != "" || contentView.text.toString() != ""){
                val time = System.currentTimeMillis()

                currentNote.title = titleView.text.toString()
                currentNote.content = contentView.text.toString()
                currentNote.updateTime = time
                if(currentNote._id != 0L){
                    currentNote.creationTime = time
                }
                viewModel.savedNote(currentNote)
            }
            else{
                Navigation.findNavController(save).popBackStack()
            }
            hideKeyboard()
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.saved.observe(requireActivity()){
            if(it){
                Toast.makeText(requireContext(),"Done",Toast.LENGTH_SHORT).show()
                Navigation.findNavController(save).popBackStack()
            }else{
                Toast.makeText(requireContext(),"Error",Toast.LENGTH_SHORT).show()
            }
        }

        viewModel.currentNote.observe(requireActivity()){
            it?.let{
                currentNote = it
                titleView.text = it.title
                contentView.text = it.content
            }
        }

        viewModel.erased.observe(requireActivity()){
            if(it)
                Navigation.findNavController(save).popBackStack()
        }
    }

    private fun hideKeyboard(){
        val imm = context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(titleView.windowToken,0)
    }
}