package com.chemasmas.globant.practicas.mycleantodoapp.framework

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.chemasmas.globant.practicas.core.data.Note
import com.chemasmas.globant.practicas.mycleantodoapp.framework.di.ApplicationModule
import com.chemasmas.globant.practicas.mycleantodoapp.framework.di.DaggerViewModelComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class NoteViewModel(application: Application):AndroidViewModel(application) {
    private val coroutineScope = CoroutineScope(Dispatchers.IO)

//    val repository = NoteRepository(RoomNoteDataSource(application))
//
//    val useCases = UseCases(
//        AddNote(repository),
//        GetNotes(repository),
//        GetNote(repository),
//        RemoveNote(repository)
//    )

    init {
        DaggerViewModelComponent.builder().applicationModule(
            ApplicationModule(getApplication())).build().inject(this)
    }

    @Inject
    lateinit var useCases:UseCases

    val saved = MutableLiveData<Boolean>()
    val erased = MutableLiveData<Boolean>()
    val currentNote = MutableLiveData<Note?>()
    fun savedNote(note:Note){
        coroutineScope.launch {
            useCases.addNote(note)
            saved.postValue(true)
        }
    }

    fun getNote(id:Long){
        coroutineScope.launch {
            val note = useCases.getNote(id)
            currentNote.postValue(note)
        }
    }

    fun deleteNote(note:Note){
        coroutineScope.launch {
            useCases.removeNote(note)
            erased.postValue(true)
        }
    }
}