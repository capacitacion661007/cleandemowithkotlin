package com.chemasmas.globant.practicas.core.usecase

import com.chemasmas.globant.practicas.core.data.Note
import com.chemasmas.globant.practicas.core.repository.NoteRepository

class AddNote(private val noteRepository: NoteRepository) {
    suspend operator fun invoke(note: Note) = noteRepository.addNote(note)
}