package com.chemasmas.globant.practicas.mycleantodoapp.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.chemasmas.globant.practicas.core.data.Note

@Entity(tableName = "note")
data class NoteEntity(
    val title: String,
    val content:String,
    @ColumnInfo("creation_date")
    val creationTime:Long,
    @ColumnInfo("updateTime")
    val updateTime:Long,
    @PrimaryKey(autoGenerate = true)
    val id:Long
){
    companion object {
        fun fromNote(note:Note) = NoteEntity(
            note.title,
            note.content,
            note.creationTime,
            note.updateTime,
            note._id
        )


    }

    fun toNote() = Note(
        title, content, creationTime, updateTime,id
    )
}