package com.chemasmas.globant.practicas.mycleantodoapp.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.chemasmas.globant.practicas.mycleantodoapp.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}