package com.chemasmas.globant.practicas.core.usecase

import com.chemasmas.globant.practicas.core.data.Note

class GetWordCount {
    operator fun invoke(note: Note):Int {
        var wc = 0
        wc += getCount(note.title)
        wc += getCount((note.content))
        return wc
    }

    private fun getCount(str:String) = str.split(" ","\n")
        .filter {
            it.contains(
                Regex(".*[a-zA-Z]].*")
            )
        }
        .count()
}