package com.chemasmas.globant.practicas.mycleantodoapp.framework

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.chemasmas.globant.practicas.core.data.Note
import com.chemasmas.globant.practicas.mycleantodoapp.framework.di.ApplicationModule
import com.chemasmas.globant.practicas.mycleantodoapp.framework.di.DaggerViewModelComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class ListNoteViewModel(application: Application): AndroidViewModel(application) {
    private val coroutineScope = CoroutineScope(Dispatchers.IO)

//    val repository = NoteRepository(RoomNoteDataSource(application))

    @Inject
    lateinit var useCases: UseCases
//    val useCases = UseCases(
//        AddNote(repository),
//        GetNotes(repository),
//        GetNote(repository),
//        RemoveNote(repository)
//    )

    init {
        DaggerViewModelComponent.builder().applicationModule(
            ApplicationModule(getApplication())
        ).build().inject(this)
    }

    val notas = MutableLiveData<List<Note>>(arrayListOf())

    fun getAllNotes(){
        coroutineScope.launch {
            val notelist = useCases.getAllNotes()
            notelist.forEach {
                it.wordCount = useCases.wordCount.invoke(it)
            }
            notas.postValue(
                notelist
            )
        }
    }
}