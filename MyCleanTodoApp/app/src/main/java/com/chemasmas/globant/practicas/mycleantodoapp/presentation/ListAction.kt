package com.chemasmas.globant.practicas.mycleantodoapp.presentation

interface ListAction {
    fun onClick(id:Long)
}