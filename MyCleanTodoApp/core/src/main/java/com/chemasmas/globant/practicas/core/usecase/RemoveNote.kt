package com.chemasmas.globant.practicas.core.usecase

import com.chemasmas.globant.practicas.core.data.Note
import com.chemasmas.globant.practicas.core.repository.NoteRepository

class RemoveNote( private val repository: NoteRepository) {
    suspend operator fun invoke(note: Note) = repository.removeNote(note)
}