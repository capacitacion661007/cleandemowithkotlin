package com.chemasmas.globant.practicas.core.data

data class Note(
    var title:String,
    var content:String,
    var creationTime:Long,
    var updateTime:Long,
    var _id:Long = 0,
    // New Functionality Word count
    var wordCount:Int = 0
)