package com.chemasmas.globant.practicas.mycleantodoapp.framework.di

import android.app.Application
import com.chemasmas.globant.practicas.core.repository.NoteRepository
import com.chemasmas.globant.practicas.mycleantodoapp.framework.RoomNoteDataSource
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule() {

    @Provides
    fun provideRepository(app: Application) = NoteRepository(RoomNoteDataSource(app))
}