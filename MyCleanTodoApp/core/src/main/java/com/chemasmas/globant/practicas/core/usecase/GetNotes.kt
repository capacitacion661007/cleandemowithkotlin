package com.chemasmas.globant.practicas.core.usecase

import com.chemasmas.globant.practicas.core.repository.NoteRepository

class GetNotes(private val repository: NoteRepository) {
    suspend operator fun invoke() = repository.getAllNotes()
}