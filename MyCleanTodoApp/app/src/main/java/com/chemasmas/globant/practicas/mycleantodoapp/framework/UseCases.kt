package com.chemasmas.globant.practicas.mycleantodoapp.framework

import com.chemasmas.globant.practicas.core.usecase.*

data class UseCases(
    val addNote: AddNote,
    val getAllNotes: GetNotes,
    val getNote: GetNote,
    val removeNote: RemoveNote,
    //New functionality
    val wordCount: GetWordCount
)
