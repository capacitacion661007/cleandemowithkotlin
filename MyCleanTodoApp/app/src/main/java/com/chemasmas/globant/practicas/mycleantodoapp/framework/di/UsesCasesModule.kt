package com.chemasmas.globant.practicas.mycleantodoapp.framework.di

import com.chemasmas.globant.practicas.core.repository.NoteRepository
import com.chemasmas.globant.practicas.core.usecase.*
import com.chemasmas.globant.practicas.mycleantodoapp.framework.UseCases
import dagger.Module
import dagger.Provides

@Module
class UsesCasesModule {
    @Provides
    fun getUseCases(repository: NoteRepository) = UseCases(
        AddNote(repository),
        GetNotes(repository),
        GetNote(repository),
        RemoveNote(repository),
        GetWordCount()
    )
}