package com.chemasmas.globant.practicas.mycleantodoapp.framework.di

import com.chemasmas.globant.practicas.mycleantodoapp.framework.ListNoteViewModel
import com.chemasmas.globant.practicas.mycleantodoapp.framework.NoteViewModel
import dagger.Component

    @Component(modules = [
    ApplicationModule::class,
    RepositoryModule::class,
    UsesCasesModule::class
])
interface ViewModelComponent {
    fun inject(noteViewModel: NoteViewModel)
    fun inject(listNoteViewModel: ListNoteViewModel)
}