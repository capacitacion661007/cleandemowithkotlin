package com.chemasmas.globant.practicas.core.repository

import com.chemasmas.globant.practicas.core.data.Note

class NoteRepository(private val datasource: NoteDatasource) {

    suspend fun addNote(note: Note) = datasource.add(note)
    suspend fun getNote(id:Long) = datasource.get(id)
    suspend fun getAllNotes() = datasource.getAll()
    suspend fun removeNote(note:Note) = datasource.remove(note)
}