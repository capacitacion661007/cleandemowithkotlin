# Concepts of clean code

## Clean architecture

A way to organize project to achieve mantainability and scalability.

One concern per component

Structured in kayer dependency

Implementation on layers depend on abstract clases

Is NOT especific to mobile development

## PROS

Strict architecture - Hard to make mistakes

Bussiness logic is encapsulated, easy to test

Enforcement of dependecies through encapsulation

Allows fro parallel development

Highly scalable}

Easy to understand and mantain

TEsting is more simple

# Layers

## Entities

Domain Objects
Foundational business logic
POJOs

## Use cases

Actions that can be taken on the entities
Depend on entities
Bussinesn logic, plain code
No other dependencies
A use case doesn't know how the result is going to be used


## Controllers, presenters, adapters

Interfaces
Retrieve data from various sources
Present data in specific format (XML, JSON)
Depend on lower level layers [Use case](#use-cases) [Entities](#entities)


# Infrastructure

How the data is interpreted and presented
Most volatile layer, likely to change
Interact with [Lower Layer](#controllers-presenters-adapters) to retrieve data
UI, frameworks , devices, etc


# Solid Principles

+ Single Responsability
+ Open-closed
+ Liskov substitution
+ Interface segregation
+ Dependency inversion

## Single responsability

A class should only have one job
One reason to change
If there are two reason to change, it should be split into two differente classes

## Open-closed

Open for extension, closed for modification
If new functionality needs to be added, it should be added to an extension of the class
Abstract away stable functionality
put volatile functionality in extension clases

## Liskov substitution

Low level classes can be substituted without affecting higher levels
Achieved using abstract clases and interfaces

## Interface segregation

Use interface to advertise functionality
Many especific interfaces are better than one generic interface
An interface only exposes the methods that the dependant class needs, not more

# Dependency inversion

Concrete classes depend on abstract classes , not the other way around
Volatile functionality depends stable functionality
Framework especific functionality depends on bussines logic

# MVVM Architecture

Supported by google, part of jetpack
Intergrates well with the Activity/Fragment life cycle
LiveData provides async comunication with the view

# About the Example

+ Note app
+ 2 modules

# Navigation

Handles how the user moves in the app
Removes complexity for moving from one screen to another
Hanldes complex cases as:

+ Bottom tabbed navigation
+ App drawers

GEnerated classes

Handles fragment transactions
Hanldes back an up actions
Manage the back stack (TODO que es?)
Argument passing
Transition animations
Deep linking

## How it works

Navigation graph
Nav host fragment
Nav controller

# Room

Abstraction layer over SQL
Easily store objects in a database
GEnerated clases
Compile time checks
Acces on backgroun thread

## Setup

@Entity
A Pojo, to be transformed to a table

@Dao
Object to provide functionality on how to access

@Database
Object // TODO fin definition

## Queries

@Insert

@Update

@Delete

@Query for custom queries

@RawQuery

## Migrations

## Unit testing functionality


# MVVM

## ViewModel

A class that stores UI related data
A bridge between the model and the view
Has a very simplified lifecycle
Should not depend on the activity context
    Can depend on activity context if necesary

## Live Data

An observable
Lifecycle aware
No memory leaks
Always up to date
Manages configuration changes

# Dagger

Componentes escenciales

+ [Modulos](#modulos)
+ Componentes
+ Proveedores

## Modulos 
@Module

Classes in which you can add bindings for types that cannot be contructor injected 
 
## Components

@Component(modules=[])

Annotates a interface or abstract class for which a fully-formed dependency-injected implementation is to be generated from a set of modules.

## Mirror COnfigurado