package com.chemasmas.globant.practicas.mycleantodoapp.framework.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query

@Dao
interface NoteDAO {

    @Insert(onConflict = REPLACE)
    suspend fun addNoteEntity(noteEntity: NoteEntity)

    @Query("Select * from note where id = :id")
    suspend fun getNoteEntity(id: Long): NoteEntity?

    @Query("Select * from note")
    suspend fun getAllNoteEntity():List<NoteEntity>

    @Delete
    suspend fun deleteNoteEntity(noteEntity: NoteEntity)
}